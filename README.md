# Personal Web

## Introduction

Welcome to the repository for my personal website! This site is built using [Zola](https://www.getzola.org/), a fast static site generator in a single binary with everything built-in.

Check out the live version of my site deployed on GitLab Pages: [My Personal Website](https://personal-web-hl467-197638aaa13046aac8035db953f3108f10d9b5831e8a.pages.oit.duke.edu).

## Deployment

The site is automatically built and deployed to GitLab Pages upon each push to the main branch.

## Walkthrough

I have created a static website using Zola, a fast static site generator. The site is hosted on GitLab Pages and is automatically deployed upon each push to the main branch. The site is responsive and ensures a seamless experience across various devices and screen sizes.
- Screenshots of the Gitlab CI/CD pipeline below:
![Gitlab CI/CD Pipeline](screenshots/cicd.png)

## Features

- **Fast and Efficient**: Built with Zola for speedy page loads and easy management.
- **Responsive Design**: Ensures a seamless experience across various devices and screen sizes.
- **GitLab Pages Deployment**: Hosted on GitLab Pages for reliability and ease of updates.

## Screen Shots

Here are a few glimpses of my static website in action:

<div style="display: flex; justify-content: center;">
    <img src="screenshots/zola1.png" alt="Screenshot 1" style="width: 45%; margin-right: 10px;">
    <img src="screenshots/zola2.png" alt="Screenshot 2" style="width: 45%;">
</div>

## Getting Started

### Prerequisites

- [Zola](https://www.getzola.org/documentation/getting-started/installation/) installed on your machine.

### Running Locally

1. Clone the repository:
   ```bash
   git clone https://gitlab.oit.duke.edu/hl467/personal-web.git 
   cd personal-web
   ```
2. Run Zola server:
   ```bash
   zola serve
   ```
   This will start a local web server. Open your browser and navigate to `http://localhost:1111`.

## Demo Video
The demo video for this project can be found [here](https://youtu.be/eSSnMob4nCE).
