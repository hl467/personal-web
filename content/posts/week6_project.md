+++
title = "Mini-Project: AWS Lambda with Logging and Tracing"
date = "2024-03-01"
+++

# Description
This project contains a Rust-written AWS Lambda function for handling user registrations. The function connects to a MySQL database to verify the uniqueness of usernames and emails and to store user information.
The structure of the project is basically the same with the previous project, but the main difference is that this project is written in Rust. For more detail about the project, please refer to the previous project's github page: https://gitlab.oit.duke.edu/risc_group1/mini5proj


# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/hl467/week6miniproject>