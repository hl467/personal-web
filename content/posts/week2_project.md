+++
title = "Mini-Project: AWS Lambda Function"
date = "2024-02-01"
+++

# Description

I created a AWS Lambda function using Rust and cargo lambda. The function responds to HTTP requests with specific greetings ("Hello" or "Hi") and returns an appropriate response. The function is deployed to AWS Lambda and is accessible via an API Gateway endpoint.

# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/hl467/aws-lambda>