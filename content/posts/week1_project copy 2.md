+++
title = "Mini-Project: Static Website"
date = "2024-01-26"
+++

# Description

A static site with Zola Links to an external site. This will hold all of the portfolio work in this class. 

# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/hl467/personal-web>