+++
title = "Mini-Project: Containerize Rust Web Service"
date = "2024-02-15"
+++

# Description
This project demonstrates the creation of a simple web application using Rust and Actix. It involves containerizing the application, building a Docker image, and running the container locally. Docker Desktop is required for local deployment.


# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/hl467/mini-proj4>