+++
title = "Mini-Project: Rust AWS Lambda with AWS RDS"
date = "2024-02-22"
+++

# Description
In this project, I created a Rust AWS Lambda function with AWS RDS database(MySQL). My AWS Lambda function is designed to handle user registration requests. It receives JSON data containing a username and an email, checks against a MySQL database for existing records, and either registers the new user or returns messages to indicate the user already exists.

# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/risc_group1/mini5proj>