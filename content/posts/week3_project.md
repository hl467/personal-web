+++
title = "Mini-Project: S3 Bucket"
date = "2024-02-08"
+++

# Description
This project demonstrates the process of creating an Amazon S3 bucket with versioning and encryption features using the AWS Cloud Development Kit (CDK). The project is developed using the local terminal, leveraging AWS IAM for permissions, and utilizes CodeWhisperer for generating CDK code.


# Delivered Link

The gitlab Ropo link is <https://gitlab.oit.duke.edu/hl467/s3bucket>